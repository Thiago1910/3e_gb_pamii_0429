import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'folder/Inbox',
    pathMatch: 'full'
  },
  {
    path: 'cad-imovel',
    loadChildren: () => import('./cad-imovel/cad-imovel.module').then( m => m.CadImovelPageModule)
  },
  {
    path: 'cad-imobiliaria',
    loadChildren: () => import('./cad-imobiliaria/cad-imobiliaria.module').then( m => m.CadImobiliariaPageModule)
  },
  {
    path: 'cad-proprietario',
    loadChildren: () => import('./cad-proprietario/cad-proprietario.module').then( m => m.CadProprietarioPageModule)
  },
  {
    path: 'cad-locador',
    loadChildren: () => import('./cad-locador/cad-locador.module').then( m => m.CadLocadorPageModule)
  },
  {
    path: 'cad-imovel',
    loadChildren: () => import('./cad-imovel/cad-imovel.module').then( m => m.CadImovelPageModule)
  },
  {
    path: 'cad-proprietario',
    loadChildren: () => import('./cad-proprietario/cad-proprietario.module').then( m => m.CadProprietarioPageModule)
  },
  {
    path: 'cad-locador',
    loadChildren: () => import('./cad-locador/cad-locador.module').then( m => m.CadLocadorPageModule)
  },
  {
    path: 'teste',
    loadChildren: () => import('./teste/teste.module').then( m => m.TestePageModule)
  },
  {
    path: 'cad-usuario',
    loadChildren: () => import('./cad-usuario/cad-usuario.module').then( m => m.CadUsuarioPageModule)
  },
  {
    path: 'adm-login',
    loadChildren: () => import('./admin/adm-login/adm-login.module').then( m => m.AdmLoginPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./admin/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'cad-usuario',
    loadChildren: () => import('./admin/usuarios/cad-usuario/cad-usuario.module').then( m => m.CadUsuarioPageModule)
  },



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
