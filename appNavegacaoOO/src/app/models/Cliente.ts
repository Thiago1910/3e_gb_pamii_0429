import { Usuario } from "./Usuario"

export class Cliente extends Usuario {
    sexo: boolean
    data_nascimento: string
    data_cadastro: string
    orientacao: string
    nome_social: string
    raca: string
    deficiencia: boolean
    descricao_deficiencia: string
}