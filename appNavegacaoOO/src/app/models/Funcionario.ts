import { Usuario } from "./Usuario"

export class Funcionario extends Usuario {
    salario: number
    data_admissao: string
    cargo: boolean
}