import { Cliente } from "./Cliente"

export class Imovel extends Cliente{
    descricao: string
    rua: string
    numero: string
    bairro: string
    cep: string
    cidade: string
    uf: string
    complemento: string
    cozinhas: number
    salas: number
    quartos: number
    banheiros: number
    vagas: number
    area_construida: number
    area_terreno: number
    tem_garagem: boolean
}