import { Pessoa } from './Pessoa'

export class Imobiliaria extends Pessoa{
    cnpj: string
    ie: string
    razao_social: string
}