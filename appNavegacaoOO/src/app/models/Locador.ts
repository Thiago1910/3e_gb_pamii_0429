export class Locador  {
    data_inicio: string
    data_termino: string
    valor_combinado: number
    tipo: boolean
    fiador: string
    ativo: boolean
}