import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdmLoginPage } from './adm-login.page';

const routes: Routes = [
  {
    path: '',
    component: AdmLoginPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdmLoginPageRoutingModule {}
