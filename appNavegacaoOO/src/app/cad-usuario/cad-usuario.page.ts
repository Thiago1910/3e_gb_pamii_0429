import { Component, OnInit } from '@angular/core';
import { Usuario } from '../models/Usuario';
import { UsuariosService } from '../services/usuarios/usuarios.service';

@Component({
  selector: 'app-cad-usuario',
  templateUrl: './cad-usuario.page.html',
  styleUrls: ['./cad-usuario.page.scss'],
})
export class CadUsuarioPage implements OnInit {
  usuario: Usuario

  constructor(
    private usuariosService: UsuariosService
  ) {
    this.usuario = new Usuario()

    this.usuario.ativo = true
    this.usuario.bairro = "Bairro app 1"
    this.usuario.cep = "18.400-00"
    this.usuario.cidade = "Cidade app 1"
    this.usuario.nome = "Usuário app 1"
    this.usuario.numero = "Num app 1"
    this.usuario.password = "app123"
    this.usuario.rua = "Rua app 1"
    this.usuario.telefone = "(15) 15151-1515"
    this.usuario.tipo = "física"
    this.usuario.uf = "SP"
    this.usuario.username = "userapp1"
  }

  ngOnInit() {
    this.usuariosService.cadastrarUsuario(this.usuario).subscribe({
      next: (resp) => {
        console.log(resp)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}
